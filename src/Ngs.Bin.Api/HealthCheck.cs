﻿using Newgistics.Lib.ServiceHost.Controller;
using Newgistics.Lib.ServiceHost.Helper;
using Newgistics.Lib.ServiceHost.Dto;
using Newgistics.Lib.MongoDb.Interfaces;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver.Core.Clusters;
using MongoDB.Bson;
using MongoDB.Driver;


namespace Ngs.Bin.Api
{
    [Controller]
    public class HealthCheck : BaseHealthCheck
    {
        private readonly string connectionString;
        private readonly string mongodbString;

        public HealthCheck(ServiceConfiguration serviceConfiguration) : base()
        {
            connectionString = serviceConfiguration.ConnectionStrings["MsSql"];
            mongodbString = serviceConfiguration.ConnectionStrings["MongoDb"];
        }
        
        public override List<HealthCheckResult> ExecuteHealthchecks()
        {
            var healthChecks = base.ExecuteHealthchecks();

                var statusResult = connectSQLServer();

                if (int.TryParse(statusResult, out var errNum))
                {
                    healthChecks.Add(new HealthCheckResult { Passed = false, CheckType = "SQLDB", Message = $"Failure Error Number: {statusResult}" });
                }
                else
                {
                    healthChecks.Add(new HealthCheckResult { Passed = true, CheckType = "SQLDB", Message = $"Status: {statusResult}" });
                }

                var mongodbResult = connectMongoDbServer();
                if(mongodbResult == 1)
                {
                 healthChecks.Add(new HealthCheckResult { Passed = true, CheckType = "MongoDB", Message = "Database is alive" });
                }
                else
                {
                  healthChecks.Add(new HealthCheckResult { Passed = false, CheckType = "MongoDB", Message = "Failed to connect to MongoDB" });
                }
                

            return healthChecks;
        }

        private string connectSQLServer()
        {
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                var objParm = new DynamicParameters();
                objParm.Add("@p_TrackingBarcodeNumber", "");
                objParm.Add("@p_RduRscfFacilityType", dbType: DbType.String, direction: ParameterDirection.Output, size: 5215585);

                try
                {
                    dbConnection.Open();
                   // dbConnection.ExecuteAsync("csp_GetParcelPRSFacilityType", objParm, commandType: CommandType.StoredProcedure);
                   // var statusResult = objParm.Get<string>("@p_RduRscfFacilityType");
                    dbConnection.Close();

                    return "ok";
                }
                catch (SqlException e)
                {
                    return e.Number.ToString();
                }
            }
        }

         private int connectMongoDbServer(){
              
            var client = new MongoClient(mongodbString);
            var database = client.GetDatabase("local");
            bool isMongoLive = database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);

if(isMongoLive)
{
    return 1;
}
else
{
    return 0;
}

        }
        
    }
}