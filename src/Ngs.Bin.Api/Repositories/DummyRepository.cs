﻿using Ngs.Bin.Api.Repositories.Interfaces;
using Newgistics.Lib.ServiceHost.Helper;
using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Dapper;

namespace Ngs.Bin.Api.Repositories
{
    public class DummyRepository : IDummyRepository
    {
        private readonly string sqlConnectionString;
        private readonly string mongoDbconnectionString;

        public DummyRepository(ServiceConfiguration serviceConfiguration)
        {
            sqlConnectionString = serviceConfiguration.ConnectionStrings["MsSql"];
            mongoDbconnectionString = serviceConfiguration.ConnectionStrings["MongoDb"];
        }

        public async Task<BooleanResult> GetDummy(string id)
        {
            var result = new BooleanResult { Success = true };
            //you better do something with your database.
            return result;
        }
    }
}
