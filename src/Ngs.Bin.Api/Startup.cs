﻿using Ngs.Bin.Api.Repositories.Interfaces;
using Ngs.Bin.Api.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Newgistics.Lib.ServiceHost.Controller;
using Newgistics.Lib.ServiceHost.Middleware;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;

namespace Ngs.Bin.Api
{
    public class Startup
    {
        private readonly IHostingEnvironment _environment;
        private readonly IConfiguration _configuration;

        public Startup(IHostingEnvironment env, IConfiguration config)
        {
            _environment = env;
            _configuration = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApiSupport(_configuration, _environment);
         //   services.AddSingleton<IFacilityRepository, FacilityRepository>();
            services.AddSingleton(typeof(BaseHealthCheck), typeof(HealthCheck));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IApplicationLifetime applicationLifetime)
        {
            app.UseStaticFiles();
            app.UseApiSupport(env, loggerFactory, applicationLifetime);
        }
    }
}
