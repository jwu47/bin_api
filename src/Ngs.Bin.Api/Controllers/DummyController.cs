﻿using Newgistics.Lib.ServiceHost.Controller;
using Newgistics.Lib.ServiceHost.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ngs.Bin.Api.Controllers
{
    [Produces("application/json")]
    public class DummyController : BaseController
    {
        public DummyController()
        {
        }        

        [HttpGet("v1/resource/{id}")]
        public IActionResult GetReource(string id)
        {
            BooleanResult result = new BooleanResult { Success = false };
            //dummy
            result.Success = true;
                    result.Message = id;
            return Ok(result );
        }

        [HttpPut("/v1/resource")]
        public IActionResult PutReource([FromBody]string value)
        {
            return Ok(new string[] { "Yet to be implemented."} );
        }
    
        [HttpPost("/v1/resource")]
        public IActionResult PostResource([FromBody]string value)
        {
            return UnavailableResult("Not built.");
        }

        [HttpDelete("/v1/resource/{id}")]
        public IActionResult DeleteResource(int id)
        {
            return UnavailableResult("Not built.");
        }

    }
}