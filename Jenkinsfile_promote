#!/usr/bin/env groovy
import groovy.json.JsonSlurper

// deploys all images that have subfolders in the ./helm folder.
// provides a dropdown in jenkins with env choices, those choices coincide with namespaces in the cluster.

podTemplate(
    label: 'promote',
    containers: [
        containerTemplate(
            name: 'jnlp',
            image: 'ngsnonprod.azurecr.io/aci_k8s/jenkins-slave-base',
            ttyEnabled: true,
            command: 'jenkins-slave',
            privileged: false,
            alwaysPullImage: true,
            workingDir: '/home/jenkins',
            args: '${computer.jnlpmac} ${computer.name}')
        ],
    volumes: [
        secretVolume(secretName: 'registrylogin', mountPath: '/registrylogin'),
        secretVolume(secretName: 'registrylogin-prod', mountPath: '/registrylogin-prod'),
        hostPathVolume(mountPath: '/usr/bin/docker', hostPath: '/usr/bin/docker'),
        hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
        hostPathVolume(mountPath: '/root/.kube', hostPath: '/root/.kube')
    ]
)

{
    node('promote') {
        try {
            NONPROD_ACR = 'registrylogin'
            PROD_ACR = 'registrylogin-prod'
            REPO_PROJ = 'mic_dnc'
            SLACK_CHANNEL = '#microservice-ci'

            stage('checkout') {
                checkout scm
            }

            stage('select image') {
                DOCKER_FILES = sh(script: "ls Dockerfile_*", returnStdout: true).tokenize()
                ALL_IMAGES = []
                for (i =0; i < DOCKER_FILES.size(); i++) {
                    ALL_IMAGES.add(i, DOCKER_FILES[i].replace("Dockerfile_",""))
                }
                OPTIONS = ALL_IMAGES.clone()
                OPTIONS.add(0,'ALL-NEWEST')

                SELECTED_IMAGE = input(
                    id: 'IMAGE_TO_PROMOTE', message: 'Promote:', parameters: [
                        [$class: 'ChoiceParameterDefinition', choices: OPTIONS.join("\n"), description: 'Promotable Images in this git Repository.  Selecting \'ALL-NEWEST\' will promote the latest version of all images.', name: 'image']
                    ]
                )
            }

            stage('select tag') {
                NONPROD_ACR_USERNAME = sh(script: "cat /${NONPROD_ACR}/username", returnStdout: true) 
                NONPROD_ACR_PASSWORD = sh(script: "cat /${NONPROD_ACR}/password", returnStdout: true) 
                NONPROD_REPO = sh(script: "cat /${NONPROD_ACR}/url", returnStdout: true) 
                sh "set +x; az login --service-principal -u `cat /${NONPROD_ACR}/azuser` -p `cat /${NONPROD_ACR}/azpassword` --tenant edc7c787-c840-48ea-bbb2-078bf7e11100"
                if (SELECTED_IMAGE == 'ALL-NEWEST') {
                    SELECTED_TAG = ""
                } else {
                    stdout = sh(script: "az acr repository show-tags -n ngsnonprod --repository ${REPO_PROJ}/${SELECTED_IMAGE} -u ${NONPROD_ACR_USERNAME} -p ${NONPROD_ACR_PASSWORD}", returnStdout: true)
                    def tags = new JsonSlurper().parseText(stdout)
                    sortedTags = sortListNewestFirst(tags)
                    
                    SELECTED_TAG = input(
                        id: 'IMAGE_TAG', message: 'tag:', parameters: [
                            [$class: 'ChoiceParameterDefinition', choices: sortedTags.join("\n"), description: 'Versions', name: 'version']
                        ]
                    )
                }
            }

            stage('push image') {
                PROD_ACR_USERNAME = sh(script: "cat /${PROD_ACR}/username", returnStdout: true)
                PROD_ACR_PASSWORD = sh(script: "cat /${PROD_ACR}/password", returnStdout: true)
                PROD_REPO = sh(script: "cat /${PROD_ACR}/url", returnStdout: true) 
                sh"""set +x
                    docker login ${NONPROD_REPO} -u ${NONPROD_ACR_USERNAME} -p ${NONPROD_ACR_PASSWORD}
                    docker login ${PROD_REPO} -u ${PROD_ACR_USERNAME} -p ${PROD_ACR_PASSWORD}
                """

                def IMAGES_TO_PROMOTE = []
                if (SELECTED_IMAGE == 'ALL-NEWEST') {
                    IMAGES_TO_PROMOTE = ALL_IMAGES
                } else {
                    IMAGES_TO_PROMOTE.add(0, SELECTED_IMAGE)
                }

                for (i =0; i < IMAGES_TO_PROMOTE.size(); i++) {
                    IMAGE_TO_PROMOTE = IMAGES_TO_PROMOTE[i]
                    if (SELECTED_TAG == "") {
                        stdout = sh(script: "az acr repository show-tags -n ngsnonprod --repository ${REPO_PROJ}/${IMAGE_TO_PROMOTE} -u ${NONPROD_ACR_USERNAME} -p ${NONPROD_ACR_PASSWORD}", returnStdout: true)
                        def tags = new JsonSlurper().parseText(stdout)
                        sortedTags = sortListNewestFirst(tags)
                        IMAGE_TAG = sortedTags[0]
                    } else {
                        IMAGE_TAG = SELECTED_TAG
                    }
                    
                    sh """
                        docker pull ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG}
                        docker tag ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG} ${PROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG}
                        docker push ${PROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG}
                        docker tag ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG} ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:promoted
                        docker push ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:promoted
                        docker tag ${NONPROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:${IMAGE_TAG} ${PROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:promoted
                        docker push ${PROD_REPO}/${REPO_PROJ}/${IMAGE_TO_PROMOTE}:promoted
                    """
                    // slackSend channel: "${SLACK_CHANNEL}", color: 'good', message: "promoted ${IMAGE_TO_PROMOTE}:${IMAGE_TAG} to ${PROD_REPO}"
                }
            }
        }
        catch (any) {
            // slackSend channel: "${SLACK_CHANNEL}", color: 'danger', message: "promotion failed ${env.JOB_NAME} ${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)"
            currentBuild.result = 'FAILURE'
            throw any //rethrow exception to prevent the build from proceeding
        }
        finally {
            step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'jwester@newgistics.com', sendToIndividuals: true])
        }
    }
}

@NonCPS
def sortListNewestFirst(List listToSort) {
    return listToSort.sort(false) {x, y ->
    def xa = x.tokenize('.'); def ya = y.tokenize('.')
    def sz = Math.min(xa.size(), ya.size())
    for (int i = 0; i < sz; i++) {
        def xs = xa[i]; def ys = ya[i];
        if (xs.isInteger() && ys.isInteger()) {
        def xn = xs.toInteger()
        def yn = ys.toInteger()
        if (xn != yn) { return xn <=> yn }
        }// else if (xs != ys) {
        //  return xs <=> ys
    // }
    // uncommenting this will make the sort more true alphanumeric, but I want the non-number tags to be at the back of the reverse sort.
    }
    return xa.size() <=> ya.size()
    }.reverse()
}
